from mesa import Agent
from utils.index import obtenerAgente, getKey


class Caja(Agent): 
    def __init__(self, model, posGrid, posMatrix, indice):
        super().__init__(model.next_id(), model)
        self.posGrid = posGrid
        self.posMatrix = posMatrix
        self.indice = indice
        self.model = model
        self.focus = False
        self.placed = False 
        self.taken = False

    def step(self):
        self.model.cajasPos[self.indice]['transportada']= self.taken
        self.model.cajasPos[self.indice]['colocada']= self.placed
        
        if not self.focus and not self.placed: 
            robot = self.encontrarRobotCecano()
            if robot:
                robot.objetivo = self
                robot.estado = "asignado"
                self.focus = True
            
            

    #Método para encontrar el robot más cercano aplicando un BFS
    def encontrarRobotCecano(self):
        direcciones = [(0,1), (1,0), (-1,0), (0,-1)]
        queue = [self.posGrid]
        dp = []
        
        while queue: 
            posActual = queue.pop()
            robot = obtenerAgente(self.model.grid, posActual)
            if robot: 
                if self.model.matrix[robot.posMatrix[0]][robot.posMatrix[1]] == 1 and robot.estado == "idle": 
                    return robot

            if posActual in dp: 
                continue

            for direccion in direcciones: 
                nuevoRow = posActual[0] + direccion[0]
                nuevoCol = posActual[1] + direccion[1]
                    
                dentroX = nuevoRow >= 0 and nuevoRow < len(self.model.matrix)
                dentroY = nuevoCol >= 0 and nuevoCol < len(self.model.matrix[0])
                    
                if dentroX and dentroY: 
                    nuevoPos = (nuevoRow, nuevoCol)
                    queue.append(nuevoPos)
                
            dp.append(posActual)
        
        return None


    def llegarCaja(self, posRobot): 
        direcciones = [(0,1), (0, -1), (1, 0), (-1,0)]
        
        for direccion in direcciones: 
            row = self.posMatrix[0] + direccion[0]
            col = self.posMatrix[1] + direccion[1]
            
            if (row >= 0 and row < len(self.model.matrix) and  col >= 0 and col < len(self.model.matrix[0])):  
                key1 = getKey((row, col))
                key2 = getKey(posRobot)
            
                if key1 == key2:
                    return True
        
        return False
            