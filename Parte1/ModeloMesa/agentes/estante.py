from mesa import Agent
from utils.index import getKey

class Estante(Agent): 
    def __init__(self, model,  id, posGrid, posMatrix,):
        super().__init__(model.next_id(), model)
        self.id = id
        self.lleno = False
        self.posGrid = posGrid
        self.posMatrix = posMatrix
        self.cantidadCajas = 0
        self.cajas = []
        self.model = model
    
    def step(self):
        self.model.estantesPos[self.id]['cajas'] = self.cajas

    def llegarEstante(self, posRobot): 
        direcciones = [(0,1), (-1, 0)]
        
        for direccion in direcciones: 
            row = self.posMatrix[0] + direccion[0]
            col = self.posMatrix[1] + direccion[1]
            
            key1 = getKey((row, col))
            key2 = getKey(posRobot)
            
            if key1 == key2:
                return True
        return False
            
            
            

    