from random import randint
from mesa import Agent
from utils.index import AStar, obtenerAgente, getKey, randomPos
from utils.index import getPosCorrectaGrid
from random import randrange


class Robot(Agent): 
    def __init__(self, model, posGrid, posMatrix, myId):
        super().__init__(model.next_id(), model)
        self.myId = str(myId) + "R"
        self.index = myId

        self.posGrid = posGrid
        self.posMatrix = posMatrix
        
        self.safePointMatrix =  posMatrix
        self.safePointGrid = posGrid
        
        self.cajaCargando = None
        
        self.bloqueo = 0

        self.model = model
        self.estado = "idle"
        
        self.objetivo = None #Posición del objetivo (Caja o estante)
        
        self.estanteriaActual = None
        self.caja = None
        self.caminoPorRecorrer = []
        self.botCercanoDetectado = False

    def step(self):
        self.model.robotPos[self.index]['x'] = self.posMatrix[0]
        self.model.robotPos[self.index]['y'] = self.posMatrix[1]
        self.model.robotPos[self.index]['estado'] = self.estado
        
        if self.estado == "idle": 
            if not self.objetivo or len(self.caminoPorRecorrer) == 0:
                self.objetivo = randomPos(self.model.matrix)
                self.caminoPorRecorrer = AStar(self.posMatrix[0], self.posMatrix[1], self.objetivo.posMatrix[0], self.objetivo.posMatrix[1], self.model.matrix, self.myId)
            
            self.desplazarse()
            
        
        if self.objetivo and self.estado == "asignado":
            if self.bloqueo == 3: 
                self.salirBloqueCaja()
            else: 
                self.model.matrix[self.objetivo.posMatrix[0]][self.objetivo.posMatrix[1]] = self.myId
                self.caminoPorRecorrer = AStar(self.posMatrix[0], self.posMatrix[1], self.objetivo.posMatrix[0], self.objetivo.posMatrix[1], self.model.matrix, self.myId)
                if len(self.caminoPorRecorrer) == 0: 
                    self.bloqueo  += 1
                elif len(self.caminoPorRecorrer) >= 2:
                    self.bloqueo = 0
                    self.caminoPorRecorrer.pop(0)
                    self.caminoPorRecorrer.pop(-1)
                    self.estado = "desplazandose"
        
        
        elif self.estado == "desplazandose": 
            if (self.bloqueo == 3): 
                self.salirBloqueCaja()
            
            if self.objetivo.llegarCaja(self.posMatrix): 
                self.model.porEliminar.append(self.objetivo)
                self.estado = "entregando"
                self.objetivo.taken = True
                self.cajaCargando = self.objetivo
                self.obtenerEstanteria()
                
            else:
                if len(self.caminoPorRecorrer) == 0: 
                    self.bloqueo += 1
                else:
                    self.bloqueo = 0
                    
                self.desplazarse()
                
        
        elif self.estado == "entregando":
                estanteria = self.objetivo
                
                if self.bloqueo == 3: 
                    self.estado = "safePoint"
                    self.caminoPorRecorrer =  AStar(self.posMatrix[0], self.posMatrix[1], self.safePointMatrix[0], self.safePointMatrix[1], self.model.matrix, self.myId, self.estado)
                    print("Toy bloqueado: ", self.caminoPorRecorrer)
                if self.objetivo.llegarEstante(self.posMatrix): 
                    self.model.cantidadCajas -= 1
                    estanteria.cantidadCajas += 1
                    if estanteria.cantidadCajas >= 5: 
                        estanteria.lleno = True
                        self.model.setEstanteria()
                    
                    self.model.matrix[self.objetivo.posMatrix[0]][self.objetivo.posMatrix[1]] = 3

                    self.objetivo.cajas.append(self.cajaCargando.indice)
                    
                    self.cajaCargando.placed = True
                    self.cajaCargando = None
                    self.objetivo = None
                    self.estado = "idle"
                    
                else: 
                    if estanteria != self.model.estanteriaPrincipal: 
                        self.model.matrix[self.objetivo.posMatrix[0]][self.objetivo.posMatrix[1]] = 3
                        self.obtenerEstanteria()
                    else: 
                        if len(self.caminoPorRecorrer) == 0:
                            self.bloqueo += 1
                            self.caminoPorRecorrer = AStar(self.posMatrix[0], self.posMatrix[1], self.objetivo.posMatrix[0], self.objetivo.posMatrix[1], self.model.matrix, self.myId)
                        
                    self.desplazarse()
        
        elif self.estado == "safePoint":
            if len(self.caminoPorRecorrer) == 0: 
                self.estado = "entregando"
            else: 
                self.desplazarse()
        
        
    def desplazarse(self):
        self.evaluarEntorno()   
        if len(self.caminoPorRecorrer) > 0: 
            self.bloqueo = 0
            self.model.matrix[self.posMatrix[0]][self.posMatrix[1]] = 0
            self.posMatrix = self.caminoPorRecorrer.pop(0)
            self.model.matrix[self.posMatrix[0]][self.posMatrix[1]] = 1
                    
            self.posGrid = (self.posMatrix[1], abs(len(self.model.matrix)-1-self.posMatrix[0]))
            self.model.grid.move_agent(self, self.posGrid)
            self.model.movimientosRealizados += 1          
    
    
    def evaluarEntorno(self): 
        direcciones = [(0,1), (0, -1), (1,0),(-1,0)]
        for direccion in direcciones:
            newRow = self.posMatrix[0] + direccion[0]
            newCol = self.posMatrix[1] + direccion[1]
            
            if (newRow >= 0 and newRow < len(self.model.matrix)) and (newCol >= 0 and newCol < len(self.model.matrix[0])):
                if self.model.matrix[newRow][newCol] == 1:
                    newKey =  getKey((newRow, newCol))
                    if len(self.caminoPorRecorrer) > 0:
                        myKey = getKey(self.caminoPorRecorrer[0])
                        if newKey == myKey: 
                            self.caminoPorRecorrer = AStar(self.posMatrix[0], self.posMatrix[1], self.objetivo.posMatrix[0], self.objetivo.posMatrix[1], self.model.matrix, self.myId)
                            if (len(self.caminoPorRecorrer) >= 2):
                                self.caminoPorRecorrer.pop(0)
                                self.caminoPorRecorrer.pop(-1)
        

    def obtenerEstanteria(self):
        self.objetivo = self.model.estanteriaPrincipal
        self.caminoPorRecorrer = AStar(self.posMatrix[0], self.posMatrix[1], self.objetivo.posMatrix[0], self.objetivo.posMatrix[1], self.model.matrix, self.myId)
        if len(self.caminoPorRecorrer)> 0:
            self.caminoPorRecorrer.pop(-1)
    
    
    def salirBloqueCaja(self): 
        self.objetivo.focus = False
        self.model.matrix[self.objetivo.posMatrix[0]][self.objetivo.posMatrix[1]] = 2
        self.objetivo = self.otraCaja()
        self.estado = "asignado"
        self.bloqueo = 0   

    def otraCaja(self): 
        direcciones = [(0,1), (1,0), (-1,0), (0,-1)]
        queue = [self.posGrid]
        dp = []
        
        while queue: 
            posActual = queue.pop()
            caja = obtenerAgente(self.model.grid, posActual)
            if caja: 
                if self.model.matrix[caja.posMatrix[0]][caja.posMatrix[1]] == 2 and  not caja.focus: 
                    return caja

            if posActual in dp: 
                continue

            for direccion in direcciones: 
                nuevoRow = posActual[0] + direccion[0]
                nuevoCol = posActual[1] + direccion[1]
                    
                dentroX = nuevoRow >= 0 and nuevoRow < len(self.model.matrix)
                dentroY = nuevoCol >= 0 and nuevoCol < len(self.model.matrix[0])
                    
                if dentroX and dentroY: 
                    nuevoPos = (nuevoCol, nuevoRow)
                    queue.append(nuevoPos)
                
            dp.append(posActual)
        
        return None
