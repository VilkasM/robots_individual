from flask import Flask
from flask.json import jsonify
from flask_restful import Api, Resource
from main import Almacen
import json

import sys
sys.path.append(".")

app = Flask(__name__)
api = Api(app)


modelado = []

@app.route("/createModel", methods=["POST"])
def create():
    modelado.append(Almacen(12, 12, 15, 5, 150))
    return 'ok', 201

@app.route("/getStepInfo", methods = ["GET"])
def stepInfo():
    if (len(modelado) == 0):
        return 'error: No hay ningún modelo', 404
    else:
        almacen = modelado[0]
        almacen.step()
        return jsonify({"cantidadCajas": almacen.cantidadCajas, "robots": almacen.robotPos, "cajas": almacen.cajasPos, "estanterias": almacen.estantesPos, "tiempoCajasCompletadas": almacen.tiempoCajasCompletadas, "tiempoTotal": almacen.tiempoMaximo, "movimientosRealizados": almacen.movimientosRealizados})


if __name__ == "__main__": 
    app.run(debug=True)
