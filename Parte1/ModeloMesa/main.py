from mesa import Model
from mesa.space import Grid
from mesa.time import BaseScheduler, RandomActivation
from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer
from random import randrange

from agentes.estante import Estante
from agentes.robot import Robot
from agentes.caja import Caja




    
class Almacen(Model):
    def __init__(self, width, height, cajas, robots, tiempoMaximo):
        super().__init__()
        
        self.tiempoMaximo = tiempoMaximo
        self.tiempoCajasCompletadas = 0
        self.movimientosRealizados = 0
        
        self.width = width
        self.height = height
        
        self.cantidadCajas = cajas
        self.cantidadRobots = robots
        
        self.robotPos = [{'x': 0, 'y': 0, 'estado': "idle", } for i in range(robots)]
        self.cajasPos = [{'x': 0, 'y': 0, 'transportada': False, 'colocada': False} for i in range(cajas)]
        self.estantesPos = [{'x': 0, 'y': 0, 'cajas': []}]
        
        self.misCajitas = []
        self.estantes = []
        self.estanteriaPrincipal = None
        
        self.porEliminar = []
        
        
        #Initialization
        self.schedule = BaseScheduler(self)
        self.grid = Grid(self.width, self.height, torus=False)
        self.matrix = [[0 for i in range(self.width)] for _ in range (self.height)]
        self.setAgents()
    
    def step(self):
        print("Cajas restantes: ", self.cantidadCajas)
        if self.tiempoMaximo > 0 and self.cantidadCajas > 0: 
            self.setEstanteria()
            self.schedule.step()
            for agente in self.porEliminar: 
                self.grid.remove_agent(agente)
                self.schedule.remove(agente)
                self.porEliminar.remove(agente)
            self.tiempoMaximo -= 1
            self.tiempoCajasCompletadas += 1
        else:
            print("Ha finalizado la ejecución")
            print("Movimientos realizados: ", self.movimientosRealizados)
            if self.cantidadCajas == 0: 
                print("Tiempo para ordenar cajas: ", self.tiempoCajasCompletadas)
                
            
    
    def setAgents(self):
        #Estantes
        self.setEstantes()
        #Robots
        self.setRobots() 
        #Cajas
        self.setCajas()
    
    
    def setEstantes(self): 
        cantidad = (self.cantidadCajas//5) + 1
        print("Cantidad; ", cantidad)
        
        for i in range(cantidad): 
            row = 0
            if len(self.estantes) == 0: 
                col = 0 
            else: 
                col = self.estantes[-1].posGrid[0] + 1

            posGrid = col, row
            posMatrix = (len(self.matrix)-1, col)
            
            estanteria = Estante(self, i, posGrid, posMatrix)
            
            self.grid.place_agent(estanteria, posGrid)
            self.schedule.add(estanteria)
            self.matrix[posMatrix[0]][posMatrix[1]] = 3
            self.estantesPos.append({'x': posMatrix[0], 'y': posMatrix[1], 'cajas': []})
            self.estantes.append(estanteria)
    
    def setRobots(self): 
        for i in range(self.cantidadRobots): 
            while True:
                row = randrange(1,len(self.matrix))
                col = randrange(1,len(self.matrix[0]))
                
                
                posGrid = (col, row)
                posMatrix = (abs(len(self.matrix)-row-1), col)
                
                if self.matrix[posMatrix[0]][posMatrix[1]] == 0: 
                    robot = Robot(self, posGrid, posMatrix, i)
                    self.robotPos[i] = {'x': posMatrix[0], 'y': posMatrix[1], "estado": "idle"}
                    self.grid.place_agent(robot, posGrid)
                    self.schedule.add(robot)
                    self.matrix[posMatrix[0]] [posMatrix[1]] = 1
                    break

            
    
    def setCajas(self): 
        for i in range(self.cantidadCajas): 
            while True:
                row = randrange(1,len(self.matrix))
                col = randrange(1,len(self.matrix[0]))
                
                
                posGrid = (col, row)
                posMatrix = (abs(len(self.matrix)-row-1), col)
                
                
                if self.matrix[posMatrix[0]][posMatrix[1]] == 0:    
                    caja = Caja(self, posGrid, posMatrix, i)
                    self.schedule.add(caja)
                    self.grid.place_agent(caja, posGrid)
                    self.matrix[posMatrix[0]][posMatrix[1]] = 2
                    self.cajasPos[i] ={'x': posMatrix[0], 'y': posMatrix[1], 'transportada': False, 'colocada': False}
                    break
    
    def setEstanteria(self): 
        indice = 0
        self.estantes.sort(key=lambda x: x.cantidadCajas, reverse=True)
        objetivo = self.estantes[0]
        while objetivo.cantidadCajas >= 5: 
            indice+=1
            objetivo = self.estantes[indice]
        
        if self.estanteriaPrincipal: 
            self.matrix[self.estanteriaPrincipal.posMatrix[0]][self.estanteriaPrincipal.posMatrix[1]] = 3
        self.matrix[objetivo.posMatrix[0]][objetivo.posMatrix[1]] = 4
        self.estanteriaPrincipal = objetivo
        
    
        
        



def agent_portrayal(agent):
    if(type(agent) == Caja):
        return {"Shape": "./images/caja.png", "r": 1, "Layer": 0}
    if(type(agent) == Robot):
        if agent.estado == "entregando" or agent.estado == "safePoint" : 
            return {"Shape": "./images/RobotOcupado.png", "r": 1, "Layer": 0}
        else: 
            return {"Shape": "./images/robot.png", "r": 1, "Layer": 0}

    if(type(agent) == Estante):  
        if agent.lleno: 
            return {"Shape": "./images/estanteLleno.png", "r": 1, "Layer": 0}
        else: 
            return {"Shape": "./images/estante.png", "r": 1, "Layer": 0}